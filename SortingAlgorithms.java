public class SortingAlgorithms {

    public static void main(String[] args) {
        System.out.println("ARRAY SORTING");
        double[] a = new double[]{5,3,2,1,8,4};
        printVector(a);

        System.out.println("USING BUBBLE SORT");
        bubbleSort(a);
        printVector(a);

        a = new double[]{5,3,2,1,8,4};
        System.out.println("USING SELECTION SORT");
        selectionSort(a);
        printVector(a);

        a = new double[]{5,3,2,1,8,4};
        System.out.println("USING INSERTION SORT");
        insertionSort(a);
        printVector(a);

        a = new double[]{5,3,2,1,8,4};
        System.out.println("USING QUICK SORT");
        quickSortSimple(a, 0, a.length-1);
        printVector(a);

        a = new double[]{31,3,7,8,29,27,10,11};
        System.out.println("USING MERGE SORT");
        mergeSort(a, 0, a.length);
        printVector(a);
    }

    private static void bubbleSort(double[] a) {
        boolean changed = true;
        for (int i = a.length - 1; i > 0 && changed; i--) {
            changed = false;
            for (int j = 0; j < i; j++) {
                if (a[j] > a[j+1]) {
                    swap(a, j, j + 1);
                    changed = true;
                }
            }
        }
    }

    private static void selectionSort(double[] a) {
        for (int i = 0; i < a.length; i++) {
            swap(a, minIndex(a, i, a.length), i);
        }
    }

    private static void insertionSort(double[] a) {
        for (int i = 1; i < a.length; i++) {
            insert(a, i);
        }
    }

    private static void insert(double[] a, int k) {
        double temp = a[k];
        int i;
        for (i = k-1; i >= 0 && a[i] > temp; i--) {
            a[i+1] = a[i];
        }
        a[i+1] = temp;
    }

    private static int minIndex(double[] a, int i, int l) {
        int m = i;
        do {
            if (a[i] < a[m]) {
                m = i;
            }
            i++;
        } while(i < l);
        return m;
    }

    private static void swap(double[] a, int i, int j) {
        double temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }


    private static void printVector(double[] a) {
        for (double d : a) {
            System.out.printf("%f ", d);
        }
        System.out.println();
    }

    private static void quickSort(double[] a, int left, int right) {
        int k;
        int m; //index of middle element
        int e; //Ende eines Trennbereiches dessen Elemente kleiner als das Trennelement sind

        if (left >= right) return;

        m = left + right >> 1; //(left + right) / 2
        swap(a, left, m); //Trennelement an Anfang des Bereiches
        e = left;

        for (k = left + 1; k < right; k++) {
            if (a[k] < a[left]) {
                swap(a, ++e, k);
            }
        }
        swap(a, e, left);  //Trennelement an das Ende des Bereiches mit Elementen < Trennwert
        quickSort(a, left, e-1); //Sort left part
        quickSort(a, e+1, right); //Sort right part
    }

    private static void quickSortSimple(double[] a, int left, int right) {
        if (left >= right) return;

        int i,j;
        double middle = a[left+right>>1];
        for (i = left, j = right; j >= 0; ) {
            while (a[i] < middle) i++; //search for element which should not be on left side
            while (a[j] > middle) j--; //search possible swap partner on right side
            if (i > j) break;

            swap(a, i++, j--);
        }

        quickSortSimple(a, left, j); //sort left side
        quickSortSimple(a, i, right); //sort right side
    }

    private static void mergeSort(double[] a, int i, int k) {
        int j;
        if (k > i + 1) {
            j = i+k >> 1;
            mergeSort(a, i, j); //sort left side
            mergeSort(a, j, k); //sort right side
            merge(a, i, j, k);
        }
    }

    private static void merge(double[] a, int i, int j, int k) {
        int m = i, n = j, l = 0;
        double[] merge = new double[a.length];

        //merging a[i]...[a[j-1] and b[j]...b[k-1]
        while (m < j && n < k) {
            if (a[m] < a[n]) merge[l++] = a[m++];
            else merge[l++] = a[n++];
        }

        while (m < j) merge[l++] = a[m++];
        while (n < k) merge[l++] = a[n++];
        for (n = 0; n < l; n++) a[i+n] = merge[n];
    }
}
